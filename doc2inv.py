#! /usr/bin/env python3

import argparse
import logging
import re
import subprocess

from csv import DictWriter
from os.path import splitext

logging.basicConfig(level=logging.WARNING)

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--infile', type=argparse.FileType('r'), help='input file')
parser.add_argument('-o', '--outfile', help='output file')
args = parser.parse_args()

FIELDNAMES = (
    'rce',
    'compilatore',
    'oggetto',
    'descrizione',
    'dimensioni',
    'stato_conservazione',
    'stima',
    'inventario_scavo',
    'tecnica',
    'cronologia',
    'provenienza',
    )

def main(infile, outfile):
    infile_type = subprocess.check_output(['file', '-b', infile.name])
    logging.info('{} :: {}'.format(infile.name, infile_type))
    if not infile_type.endswith(b'text\n'):
        try:
            subprocess.check_call(['unoconv', '-f', 'txt', infile.name])
        except subprocess.CalledProcessError:
            logging.error('Error converting file {}'.format(infile.name))
        else:
            infile = open(infile.name.replace('.doc', '.txt'))
    with infile as f:
        schede = []
        r = {}
        for l in f:
            if l.startswith('MINISTERO'):
                r = {}
            if l.startswith('Oggetto'):
                r['oggetto'] = l[8:].strip()
            if l.startswith('Descrizione'):
                r['descrizione'] = l[12:].strip()
            if l.startswith('Dimensioni'):
                r['dimensioni'] = l[11:].strip()
            if l.startswith('Tecnica'):
                r['tecnica'] = l[8:].strip()
            if l.startswith('Cronologia'):
                r['cronologia'] = l[11:].strip()
            if l.startswith('Provenienza'):
                r['provenienza'] = l[12:].strip()
            if l.startswith('Inventario scavo'):
                r['inventario_scavo'] = l[17:].strip()
            if l.startswith('R.C.G.E.'):
                r['rce'] = l[9:].strip()
            if l.startswith('5) STATO DI CONSERVAZIONE'):
                r['stato_conservazione'] = next(f).strip()
            if 'stima' in l.lower():
                try:
                    r['stima'] = re.search(r'(\d+)', l).group(0)
                except AttributeError:
                    logging.error('Errore nel file {}: campo STIMA'.format(infile.name))
            if 'Compilatore' in l:
                r['compilatore'] = next(f).strip()
            if l.startswith('Visto:'):
                schede.append(r)
        schede_sorted = sorted(schede, key=lambda s: s['rce'], reverse=True)
        if not outfile:
            csvname = splitext(f.name)[0] + '.csv'
            with open(csvname, 'w') as c:
                dw = DictWriter(c, fieldnames=FIELDNAMES)
                dw.writeheader()
                dw.writerows(schede_sorted)
        else:
            csvname = outfile
            with open(csvname, 'a+') as c:
                dw = DictWriter(c, fieldnames=FIELDNAMES)
                c.seek(0)
                if c.read() == '':
                    dw.writeheader()
                dw.writerows(schede_sorted)

if __name__ == '__main__':
    if not args.outfile:
        outfile = '{}.csv'.format(args.infile.name.rsplit('.', 1)[0])
    else:
        outfile = args.outfile
    main(args.infile, outfile)
