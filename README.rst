=================================================
 Estrai schede di inventario da documenti ``.doc``
=================================================

Ti consegnano delle schede di inventario in formato ``.doc`` (o ``.docx``
o anche ``.odt``, insomma un documento di testo). A te però servivano in
una tabella, perché ovviamente i dati inventariali vanno inseriti in
una banca dati, non stampati su un foglio di carta.

Requisiti
=========

Usa un sistema operativo GNU/Linux, tipo Debian, Ubuntu o Fedora.

Installa ``unoconv``, il programma di conversione da linea di comando
che usa LibreOffice per convertire decine di formati.


Istruzioni per l'uso
====================

Possono averti dato un file per ogni scheda inventariale, oppure un
file con molte schede una dietro l'altra. Non fa differenza. Il primo
passaggio è la conversione del documento in formato testo semplice
con il comando::

  unoconv -f txt "Scheda inventariale in formato.doc"

Il risultato è il file ``Scheda inventariale in formato.txt`` nella
stessa directory del file originale.

Lo script ``doc2inv.py`` è in grado di automatizzare anche questo
passaggio, richiamando internamente ``unoconv`` se il file passato
come input non è di tipo testo semplice.

La conversione in tabella CSV può quindi essere ottenuta direttamente
con il comando::

  ./doc2inv.py -i "Scheda inventariale in formato.doc"

e verrà creato un file `Scheda inventariale in formato.csv` sempre
nella stessa directory (comparirà anche il file intermedio
`Scheda inventariale in formato.txt`). Puoi automatizzare
l'operazione con un normale comando nella shell::

  find . -name "*.doc" -print0 | xargs -I '{}' -0 ./doc2inv.py -i {}

Combinare più file in una tabella
---------------------------------

Per combinare più file in una unica tabella, fornire anche il nome del
file di output::

  find . -name "*.doc" -print0 | xargs -I '{}' -0 ./doc2inv.py -o tabella.csv -i {}

I dati estratti dai singoli file verranno combinati in una unica tabella.
